package com.rpm;

import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        double[] allXOne = {7.6, 2.2, -1.3};
        double[] allXTwo = {0.5, 9.1, 0.2};
        double[] allXThree = {2.4, 4.4, 5.8};
        double[] allResult = {1.9, 9.7, -1.4};
        System.out.println("Линейное уравнение:");
        outPut(allXOne, allXTwo, allXThree, allResult);
        change(allXOne, allXTwo, allXThree);//переносим элементы
        System.out.println("Находим 10x:");
        firstStep(allXOne, allXTwo, allXThree);
        outPutSteps(allXOne, allXTwo, allXThree, allResult);
        System.out.println("Делим на 10:");
        secondStep(allXOne, allXTwo, allXThree, allResult);
        outPutSteps(allXOne, allXTwo, allXThree, allResult);

        double[] x = new double[3];
        for (int i = 0; i < 3; i++) {
            x[i] = allResult[i];
        }

        System.out.println("Выводим x:");
        outPutXs(allXOne, allXTwo, allXThree, allResult, x);

        double[] allXs = iteration(allXOne, allXTwo, allXThree, allResult, x);
        System.out.println("cout=" + doIteration(allXOne, allXTwo, allXThree, allResult, x, allXs));

    }

    private static void outPut(double[] allXOne, double[] allXTwo, double[] allXThree, double[] allResult) {
        for (int i = 0; i < 3; i++) {
            System.out.println(allXOne[i] + "*X1 + " + allXTwo[i] + "*X2 + " + allXThree[i] + "*X3 = " + allResult[i]);
        }
    }

    private static void firstStep(double[] allXOne, double[] allXTwo, double[] allXThree) {
        allXOne[0] = 10 - allXOne[0];
        allXTwo[1] = 10 - allXTwo[1];
        allXThree[2] = 10 - allXThree[2];
    }

    private static void secondStep(double[] allXOne, double[] allXTwo, double[] allXThree, double[] allResult) {
        for (int i = 0; i < 3; i++) {
            allXOne[i] = allXOne[i] / 10;
            allXTwo[i] = allXTwo[i] / 10;
            allXThree[i] = allXThree[i] / 10;
            allResult[i] = allResult[i] / 10;
        }
    }

    private static void change(double[] allXOne, double[] allXTwo, double[] allXThree) {
        allXTwo[0] *= -1;
        allXThree[0] *= -1;
        allXOne[1] *= -1;
        allXThree[1] *= -1;
        allXOne[2] *= -1;
        allXTwo[2] *= -1;
    }

    private static void outPutSteps(double[] allXOne, double[] allXTwo, double[] allXThree, double[] allResult) {
        for (int i = 0; i < 3; i++) {
            //  System.out.println("X" + (i + 1) + " = " + allResult[i] + " - " + allXOne[i] + "*X1 - " + allXTwo[i] + "*X2 - " + allXThree[i] + "*X3");
            System.out.printf("X%d = %.3f + %.3f*X1 + %.3f*X2 + %.3f*X3 \n", (i + 1), allResult[i], allXOne[i], allXTwo[i], allXThree[i]);
        }
    }

    private static double[] iteration(double[] allXOne, double[] allXTwo, double[] allXThree, double[] allResult, double[] x) {
        double x1 = allResult[0] + allXOne[0] * x[0] + allXTwo[0] * x[1] + allXThree[0] * x[2];
        double x2 = allResult[1] + allXOne[1] * x[0] + allXTwo[1] * x[1] + allXThree[1] * x[2];
        double x3 = allResult[2] + allXOne[2] * x[0] + allXTwo[2] * x[1] + allXThree[2] * x[2];


        double[] allXs = {x1, x2, x3};

        return allXs;
    }

    private static void outPutXs(double[] allXOne, double[] allXTwo, double[] allXThree, double[] allResult, double[] x) {
        double[] allXs = iteration(allXOne, allXTwo, allXThree, allResult, x);
        System.out.printf("X%d = %.3f + %.3f*%.3f + %.3f*%.3f + %.3f*%.3f \n", 1, allResult[0], allXOne[0], x[0], allXTwo[0], x[1], allXThree[0], x[2]);
        System.out.printf("X%d = %.3f + %.3f*%.3f + %.3f*%.3f + %.3f*%.3f \n", 2, allResult[1], allXOne[1], x[0], allXTwo[1], x[1], allXThree[1], x[2]);
        System.out.printf("X%d = %.3f + %.3f*%.3f + %.3f*%.3f + %.3f*%.3f \n", 3, allResult[2], allXOne[2], x[0], allXTwo[2], x[1], allXThree[2], x[2]);
        for (int i = 0; i < 3; i++) {
            System.out.printf("X%d = %.3f \n", (i + 1), allXs[i]);
        }
    }

    private static void comparing(double[] x, double[] allXs) {
        int tempX[] = new int[3];
        int tempAllXs[] = new int[3];
        for (int i = 0; i < 3; i++) {
            tempAllXs[i] = (int) Math.round(allXs[i]);
            tempX[i] = (int) Math.round(x[i]);
        }
        if (tempX[0] != tempAllXs[0] && tempX[1] != tempAllXs[1] && tempX[2] != tempAllXs[2]) {
            x[0] = allXs[0];
            x[1] = allXs[1];
            x[2] = allXs[2];
        }

    }


    private static int doIteration(double[] allXOne, double[] allXTwo, double[] allXThree, double[] allResult, double[] x, double[] allXs) {
        int count = 0;
        do {

            comparing(x, allXs);
            iteration(allXOne, allXTwo, allXThree, allResult, x);
            //  outPutXs(allXOne, allXTwo, allXThree, allResult, x);
            count++;

        } while (x[0] != allXs[0] && x[1] != allXs[1] && x[2] != allXs[2]);
        return count;
    }

}
