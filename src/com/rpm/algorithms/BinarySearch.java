package com.rpm.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class BinarySearch {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = new int[15];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 15);
        }

        System.out.print("find: ");
        int key = scanner.nextInt();

        System.out.println("Array:");
        System.out.println(Arrays.toString(array));
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));

        int index = binarySearch0(array, key);
        if (index == -1) {
            System.out.println("not found");
        } else {
            System.out.println(key + "[" + index + "]");
        }
//***
        ArrayList<Integer> listArray = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            listArray.add((int) (Math.random() * 15));
        }

        System.out.println("\nArrayList:");
        System.out.println(listArray.toString());
        Collections.sort(listArray);
        System.out.println(listArray.toString());

        int indexList = binarySearch0(listArray, key);
        if (indexList == -1) {
            System.out.println("not found");
        } else {
            System.out.println(key + "[" + indexList + "]");
        }
    }

    private static int binarySearch0(int[] array, int key) {
        int low = 0;
        int high = array.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            int midVal = array[mid];
            if (midVal == key) {
                return mid;
            }

            if (midVal < key) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;  // key not found.
    }

    private static int binarySearch0(ArrayList<Integer> listArray, int key) {
        int low = 0;
        int high = listArray.size() - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            int midVal = listArray.get(mid);
            if (midVal == key) {
                return mid;
            }

            if (midVal < key) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;  // key not found.
    }
}
