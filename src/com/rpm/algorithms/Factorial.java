package com.rpm.algorithms;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Класс для представления метода рассчета факториала числа
 *
 * @author Mashina A.A.
 */
public class Factorial {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println(recursFactorial(5));
        System.out.print("Введите число: ");

        int number = scanner.nextInt();

        if (number < 0 || number % 1 != 0) {
            System.out.println("VASYA VSE FIGNYA DAVAI PO NOVOI");
        } else {
            System.out.println(number + "! = " + factorial(number));
        }
    }

    /**
     * Вычисляет факториал
     *
     * @param number неотрицательное число
     * @return факториал неотрицательного числа
     */
    private static BigInteger factorial(int number) {
        if (number == 1 || number == 0) return BigInteger.ONE;

        BigInteger factorials = BigInteger.ONE;
        for (int i = 2; i <= number; i++) {
            factorials = factorials.multiply(BigInteger.valueOf(i));
        }
        return factorials;
    }

    private static long recursFactorial(int number) {
        if (number == 1 || number == 0) return 1;

        return recursFactorial(number - 1) * number;
    }
}
