package com.rpm.algorithms;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Реализация сортировки вставками
 *
 * @author Mashina A
 */

public class Inserting {
    public static void main(String[] args) {
        int[] array = new int[9];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
        System.out.println(Arrays.toString(array));
        inserting(array);
        System.out.println(Arrays.toString(array));
        System.out.println();

        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            arrayList.add((int) (Math.random() * 10));
        }
        System.out.println(arrayList.toString());
        inserting(arrayList);
        System.out.println(arrayList.toString());
    }

    /**
     * сортирует массив элементов
     *
     * @param array массив элементов
     */
    private static void inserting(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int temp = array[i];
            int in = i;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;
        }
    }

    /**
     * сортирует ArrayList
     *
     * @param arrayList ArrayList
     */
    private static void inserting(ArrayList<Integer> arrayList) {
        int length = arrayList.size();
        for (int i = 1; i < length; i++) {
            int temp = arrayList.get(i);
            int in = i;
            while (in > 0 && arrayList.get(in - 1) >= temp) {
                arrayList.set(in, arrayList.get(in - 1));
                in--;
            }
            arrayList.set(in, temp);
        }
    }
}
