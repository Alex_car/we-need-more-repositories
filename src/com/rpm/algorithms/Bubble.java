package com.rpm.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Реализация пузырьковой сортировки
 *
 * @author Mashina A
 */
public class Bubble {
    public static void main(String[] args) {
        int[] selection = new int[9];

        for (int i = 0; i < selection.length; i++) {
            selection[i] = (int) (Math.random() * 10);
        }
        //int [] selection={7, 3, 5, 1, 3, 3, 3, 8, 1};
        System.out.println(Arrays.toString(selection));
        bubble2(selection);
        System.out.println(Arrays.toString(selection));
        System.out.println();

        ArrayList<Integer> selectionList = new ArrayList<>();
        for (int i = 0; i < selection.length; i++) {
            selectionList.add((int) (Math.random() * 10));
        }
        System.out.println(selectionList.toString());
        Collections.sort(selectionList);
        System.out.println(selectionList.toString());
    }

    /**
     * моя реализация
     *
     * @param selection массив элементов
     */
    private static void bubble(int[] selection) {
        for (int i = 0; i < selection.length; i++) {
            int length = selection.length;
            int index = 0;

            for (int j = index + 1; j < length; j++) {

                if (selection[index] > selection[j]) {
                    int temp = selection[index];
                    selection[index] = selection[j];
                    selection[j] = temp;
                }

                index++;
            }
            length--;
        }
    }

    /**
     * нормальная реализация
     *
     * @param selection массив элементов
     */
    private static void bubble2(int[] selection) {
        for (int i = selection.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (selection[j] > selection[j + 1]) {
                    int temp = selection[j];
                    selection[j] = selection[j + 1];
                    selection[j + 1] = temp;
                }
            }
        }
    }
}
