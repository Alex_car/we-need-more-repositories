package com.rpm.algorithms;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Реализация сортировки выбором
 *
 * @author Mashina A
 */

public class NumberSorting {
    public static void main(String[] args) {
        int[] selection = new int[9];

        for (int i = 0; i < selection.length; i++) {
            selection[i] = (int) (Math.random() * 10);
        }
        System.out.println(Arrays.toString(selection));
        sorting(selection);
        System.out.println(Arrays.toString(selection));
        System.out.println();

        ArrayList<Integer> selectionList = new ArrayList<>();
        for (int i = 0; i < selection.length; i++) {
            selectionList.add((int) (Math.random() * 10));
        }
        System.out.println(selectionList.toString());
        sorting(selectionList);
        System.out.println(selectionList.toString());
    }

    /**
     * сортирует массив
     *
     * @param selection массив элементов
     */
    private static void sorting(int[] selection) {
        for (int i = 0; i < selection.length - 1; i++) {
            int minI = i;
            for (int j = i + 1; j < selection.length; j++) {
                if (selection[minI] > selection[j]) {
                    minI = j;
                }
            }
            int first = selection[i];
            selection[i] = selection[minI];
            selection[minI] = first;
        }
    }

    /**
     * сортирует ArrayList
     *
     * @param selectionList ArrayList
     */
    private static void sorting(ArrayList<Integer> selectionList) {
        int length = selectionList.size();
        for (int i = 0; i < length - 1; i++) {
            int minI = i;
            for (int j = i + 1; j < length; j++) {
                if (selectionList.get(minI) > selectionList.get(j)) {
                    minI = j;
                }
            }
            int first = selectionList.get(i);
            selectionList.set(i, selectionList.get(minI));
            selectionList.set(minI, first);
        }
    }

}
