package com.rpm.algorithms;

import java.util.Scanner;
import java.util.ArrayList;

/**
 * Реализация нахождения элемента в массиве
 *
 * @author Mashina A
 */
public class Algorithms {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(3);
        numbers.add(8);
        numbers.add(1);
        numbers.add(7);
        numbers.add(2);
        numbers.add(0);
        numbers.add(4);
        numbers.add(6);
        numbers.add(9);

        int[] numb = {5, 0, 2, 8, 3, 6, 1, 9, 7, 4};
        System.out.print("Введите число для поиска: ");
        int num = scanner.nextInt();

        System.out.println("Массив чисел:");
        OutputMassive(numb);
        System.out.println("");
        if (SequentialSearch(numb, num) == -1) {
            System.out.println("элемент " + num + " не найден");
        } else {
            System.out.println("элемент " + num + ", индекс " + SequentialSearch(numb, num));
        }

        System.out.println("ArrayList чисел:");
        OutputArray(numbers);
        System.out.println("");
        if (SequentialArraySearch(numbers, num) == -1) {
            System.out.println("элемент " + num + " не найден");
        } else {
            System.out.println("элемент " + num + ", индекс " + SequentialArraySearch(numbers, num));
        }
    }

    /**
     * Находит индекс введенного числа в массиве, если оно есть
     *
     * @param numb масиив чисел
     * @param num  введенное число
     * @return индекс введенного числа в массиве
     */
    public static int SequentialSearch(int[] numb, int num) {
        for (int index = 0; index < numb.length; index++) {
            if (numb[index] == num) {
                return index;
            }
        }
        return -1;
    }

    /**
     * Выводит массив чисел
     *
     * @param numb массив чисел
     */
    public static void OutputMassive(int[] numb) {
        for (int i = 0; i < numb.length; i++) {
            System.out.print(numb[i] + " ");
        }
    }

    /**
     * Выводит массив чисел
     *
     * @param numbers массив чисел
     */
    public static void OutputArray(ArrayList<Integer> numbers) {
        int size = numbers.size();
        for (int i = 0; i < size; i++) {
            System.out.print(numbers.get(i) + " ");
        }
    }

    /**
     * Находит индекс введенного числа в массиве, если оно есть
     *
     * @param numbers массив чисел
     * @param num     введенное число
     * @return индекс введенного числа в массиве
     */
    public static int SequentialArraySearch(ArrayList<Integer> numbers, int num) {
        int size = numbers.size();
        for (int index = 0; index < size; index++) {
            if (numbers.get(index) == num) {
                return index;
            }
        }
        return -1;
    }
}

