package com.rpm.algorithms.person;

public class Person {
    private  String surname;
    private  int year;

    public Person(String surname, int year) {
        this.surname = surname;
        this.year = year;
    }

    public Person() {
        this("Ололоев",1950);
    }

    @Override
    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", year=" + year +
                '}';
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }
}
