package com.rpm.algorithms.person;
/**do Person
 * @Autor Im
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Person person1 = new Person("Влололо", 1999);
        Person person2 = new Person("Алалала", 2000);
        Person person3 = new Person("Блулулу", 1958);
        Person person4 = new Person("Еаркар", 2018);
        Person person5 = new Person("Влололо", 1996);
        Person[] people = {person1, person2, person3, person4, person5};
        out(people);
        //sorting(people);
        // bubbleSorting(people);
         inSorting(people);
        System.out.println();
        out(people);
        System.out.println("\nsurname:");
        String person = scanner.nextLine();
        findOut(people, person);
    }

    /**sorting ppl
     *
     * @param people massive of ppl
     */
    private static void sorting(Person[] people) {
        for (int i = 0; i < people.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < people.length; j++) {
                int isCompare = people[min].getSurname().compareTo(people[j].getSurname());
                if (isCompare > 0) {
                    min = j;
                }
            }
            if (i != min) {
                Person temp = people[i];
                people[i] = people[min];
                people[min] = temp;
            }
        }
    }

    /**sorting ppl too
     *
     * @param people massive of ppl
     */
    private static void bubbleSorting(Person[] people) {
        for (int i = 0; i < people.length; i++) {
            for (int j = i + 1; j < people.length; j++) {
                int isCompare = people[i].getSurname().compareTo(people[j].getSurname());
                if (isCompare > 0) {
                    Person temp = people[i];
                    people[i] = people[j];
                    people[j] = temp;
                    continue;
                }
            }
        }
    }

    /**too sorting ppl too
     *
     * @param people massive of ppl
     */
    private static void inSorting(Person[] people) {
        for (int i = 1; i < people.length; i++) {
            Person temp = people[i];
            int in = i;
            while (in > 0 && people[in - 1].getYear() <= temp.getYear()) {
                people[in] = people[in - 1];
                in--;
            }
            people[in] = temp;
        }

    }

    /**sout massive of ppl
     *
     * @param people massive of ppl
     */
    private static void out(Person[] people) {
        for (int i = 0; i < people.length; i++) {
            System.out.println(people[i] + " ");
        }
    }

    /**finding ppl in massive of ppl
     *
     * @param people massive of ppl
     * @param person ppl whom we need to find
     */
    private static ArrayList<Person> finding(Person[] people, String person) {
        ArrayList <Person> samePerson = new ArrayList<>();
        for (int i = 0; i < people.length; i++) {
            if (person.equalsIgnoreCase(people[i].getSurname())) {
                samePerson.add(people[i]);
            }
        }
        return samePerson;
    }

    /**sout find ppl if they are
     *
     * @param people massive of ppl
     * @param person ppl whom we need to find
     */
    private static void findOut (Person[] people, String person){
        ArrayList <Person> samePerson = finding(people,person);
        int size = samePerson.size();
        if (samePerson.isEmpty()){
            System.out.println("not found");
        } else {
            for (int i = 0; i < size; i++) {
                System.out.println(samePerson.get(i) + " ");
            }
        }
    }
}
